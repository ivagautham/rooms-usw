//
//  ViewController.swift
//  Rooms@USW
//
//  Created by Gautham Velappan on 9/5/19.
//  Copyright © 2019 Gautham Velappan. All rights reserved.
//

import UIKit
import JTAppleCalendar

class ViewController: UIViewController {

    @IBOutlet var monthTitle: UILabel!
    @IBOutlet var calendar: JTACMonthView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendar.scrollDirection = .vertical
        calendar.scrollingMode = ScrollingMode.stopAtEachSection
        calendar.showsHorizontalScrollIndicator = false
    }

    func configureCell(view: JTACDayCell?, cellState: CellState) {
        guard let cell = view as? DateCell  else { return }
       
        cell.dateLabel.text = cellState.text
        cell.selectedView.layer.cornerRadius = 3.0
        
        handleCellTextColor(cell: cell, cellState: cellState)
        handleCellSelected(cell: cell, cellState: cellState)
    }
    
    func handleCellTextColor(cell: DateCell, cellState: CellState) {
        cell.dateLabel.textColor = (cellState.dateBelongsTo == .thisMonth) ? UIColor.black : UIColor.gray
    }

    func handleCellSelected(cell: DateCell, cellState: CellState) {
        cell.selectedView.isHidden = !cellState.isSelected
    }

}


extension ViewController: JTACMonthViewDataSource {
    
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        let startDate = Date().lastYearToday
        let endDate = Date().nextYearToday
        let numberOfRows = 6
        let calendar = Calendar.current
        let generateInDates = InDateCellGeneration.forAllMonths
        let generateOutDates = OutDateCellGeneration.tillEndOfGrid
        let firstDayOfWeek = DaysOfWeek.sunday
        let hasStrictBoundaries = true
        
        return ConfigurationParameters(startDate: startDate,
                                       endDate: endDate,
                                       numberOfRows: numberOfRows,
                                       calendar: calendar,
                                       generateInDates: generateInDates,
                                       generateOutDates: generateOutDates,
                                       firstDayOfWeek: firstDayOfWeek,
                                       hasStrictBoundaries: hasStrictBoundaries)
    }

}

extension ViewController: JTACMonthViewDelegate {
   
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
  
    func calendar(_ calendar: JTACMonthView, shouldSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) -> Bool {
        return true
    }
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! DateCell
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        return cell
    }
    
}

extension ViewController {
   
    func calendar(_ calendar: JTACMonthView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTACMonthReusableView {
        let formatter = DateFormatter()  // Declare this outside, to avoid instancing this heavy class multiple times.
        formatter.dateFormat = "MMMM, YYYY"
        monthTitle.text = formatter.string(from: range.start)

        return calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "DateHeader", for: indexPath)
    }

    func calendarSizeForMonths(_ calendar: JTACMonthView?) -> MonthSize? {
        return MonthSize(defaultSize: 1.0)
    }

}

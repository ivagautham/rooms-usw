//
//  Date+Utilities.swift
//  COMPASS Mobile
//
//  Created by Gautham Velappan on 1/16/19.
//  Copyright © 2019 Gautham Velappan. All rights reserved.
//

import Foundation

enum DateFormatPattern: String {
    case defaultDate = "MM/dd/yyyy"
    case defaultTime = "h:mm a"
    case meridianTimePattern = "a"
    case hourMinTimePattern = "h:mm"
    case militaryTimePattern = "HH:mm"
    case fullDate = "MMMM d, yyyy"
    case fullDateWithoutComma = "MMMM d yyyy"
    case shortDate = "MMM d, yyyy"
    case runningDate = "MMddyyyy"
    case runningYear = "yyyyMMdd"
    case longDateFormate = "yyyy-MM-dd HH:mm:ss ZZZ"
    case extendedDateFormate = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
    case shortYearFormat = "MM/dd/YY"
    case timestampFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case recentOrdersDate = "yyyy-MM-dd'T'HH:mm:ssZ"
    case weekDayDate = "E, MMM d"
    case transactionDate = "yyyy-MM-dd'T'HH:mm:ss"
    case transactionShortDate = "MMdd"
    case dayDateTime = "E, d MMM yyyy h:mm a"
    case statusHealthFormat = "M/d/YY h:mm a"
    case depositDate = "yyMMdd"
    
    // 2016-11-30T23:59:59.000-05:00
    case updatingDate = "yyyy-MM-dd"
    
    case issueDate = "dd-MMM-yyyy"
    
    // 04/12/2019T02:45 PM
    case receiptDate = "MM/dd/yyyy'T'HH:mm:ss"
    case receiptTranscationDate = "MM/dd/yy"
    
    //    var localizedValue: String {
    //        return NSLocalizedString(rawValue, comment: "")
    //    }
}

extension Date {
    // ============================================================
    // === Internal API ===========================================
    // ============================================================
    
    // MARK: - Internal API -
    
    // MARK: Internal Properties
    
    //    var minsAgoSinceNow: Double {
    //        let calendar = Calendar.current
    //        let now = Date()
    //
    //        let earliest = now < self ? now : self
    //        let latest = (earliest == now) ? self : now
    //
    //        var set = Set<Calendar.Component>()
    //        set.insert(.minute)
    //
    //        let components = calendar.dateComponents([.minute], from: earliest, to: latest)
    //        let minute = components.minute ?? 0
    //
    //        return Double(minute)
    //    }
    //
    //    var timeAgoSinceNow: String {
    //        let calendar = Calendar.current
    //        let now = Date()
    //
    //        let earliest = now < self ? now : self
    //        let latest = (earliest == now) ? self : now
    //
    //        var set = Set<Calendar.Component>()
    //        set.insert(.minute)
    //
    //        let components = calendar.dateComponents([.second, .minute, .hour, .day, .weekOfYear, .month, .year], from: earliest, to: latest)
    //
    //        let year = components.year ?? 0
    //        let month = components.month ?? 0
    //        let weekOfYear = components.weekOfYear ?? 0
    //        let day = components.day ?? 0
    //        let hour = components.hour ?? 0
    //        let minute = components.minute ?? 0
    //        let second = components.second ?? 0
    //
    //        if year >= 1 {
    //            return "\(year)y ago"
    //        } else if month >= 1 {
    //            return "\(month)mo ago"
    //        } else if weekOfYear >= 1 {
    //            return "\(weekOfYear)w ago"
    //        } else if day >= 1 {
    //            return "\(day)d ago"
    //        } else if hour >= 1 {
    //            return "\(hour)h ago"
    //        } else if minute >= 1 {
    //            return "\(minute)m ago"
    //        } else if second >= 1 {
    //            return "\(second)s ago"
    //        } else {
    //            return "Just now"
    //        }
    //    }
    
    /**
     Returns a string for a given date
     
     :param: date (Required) The date to be converted to a string.
     :param: dateFormatPattern (Default) The date format of the required string
     
     :retruns: string for a given date
     
     :example:
     1.
     var dateString = NSDate.stringForDate(NSDate(), dateFormatPattern: .defaultTime)
     */
    static func stringForDate(date: Date, dateFormatPattern: DateFormatPattern? = .timestampFormat) -> String {
        let dateFormatter = DateFormatter()
        // Do not localize dateFormatPattern
        dateFormatter.dateFormat = dateFormatPattern!.rawValue
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    /**
     Returns a date for a given string
     
     :param: string (Required) The string to be converted to a NSDate object.
     :param: dateFormatPattern (Default) The date format for the required date
     
     :retruns: date for a given string
     
     :example:
     1.
     var currentDate = dateFromString(String("6:30 AM"), dateFormatPattern: .defaultTime)
     */
    static func dateFromString(dateString: String, dateFormatPattern: DateFormatPattern? = .timestampFormat) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatPattern!.rawValue
        if let date = dateFormatter.date(from: dateString) {
            return date
        }
        
        return Date()
    }
    
    @discardableResult
    static func tomorrow() -> Date {
        let cal = NSCalendar.current
        let startDate = cal.startOfDay(for: Date())
        let date = cal.date(byAdding: .day, value: +1, to: startDate)
        return date ?? Date()
    }
    
    var lastYearToday: Date {
        return Calendar.current.date(byAdding: .month, value: -12, to: Date()) ?? Date()
    }

    var nextYearToday: Date {
        return Calendar.current.date(byAdding: .month, value: 12, to: Date()) ?? Date()
    }
   
}

//extension Date {
//    func differenceBetweenDates(lastLoggedInDate: Date) -> (Int?) {
//        let dateToday = Date()
//        return Calendar.current.dateComponents([.hour], from: lastLoggedInDate, to: dateToday).hour!
//    }
//
//    func differenceBetweenDatesWithNumberofDays() -> (Int?) {
//        return Calendar.current.dateComponents([.day], from: Calendar.current.date(byAdding: .month, value: -1, to: Date())!, to: Date()).day!
//    }
//
//    func differenceBetweenDatesWithNumberofDaysWithDate(fromDate: Date, toDate: Date) -> (Int?) {
//        return Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day!
//    }
//}

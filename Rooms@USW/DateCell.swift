//
//  DateCell.swift
//  Rooms@USW
//
//  Created by Gautham Velappan on 9/5/19.
//  Copyright © 2019 Gautham Velappan. All rights reserved.
//

import UIKit
import JTAppleCalendar

class DateCell: JTACDayCell {
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var selectedView: UIView!

}
